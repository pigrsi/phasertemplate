var MyGameMaps = {
    loadMap: function() {  
        var map = scene.add.tilemap('testmap_0');
        var tileset = map.addTilesetImage('green_ground','green_square');
        var collideLayer = map.createStaticLayer('layer1', tileset, 0, 0);
        collideLayer.setCollision(1);
        
        /*const debugGraphics = scene.add.graphics().setAlpha(0.75);
        collideLayer.renderDebug(debugGraphics, {
            tileColor: null, // Color of non-colliding tiles
            collidingTileColor: new Phaser.Display.Color(10, 10, 10, 200), // Color of colliding tiles
            faceColor: new Phaser.Display.Color(40, 39, 37, 255) // Color of colliding face edges
        });*/
        
        return collideLayer;
    }
}