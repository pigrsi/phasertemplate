var MyGameWorld = {};
MyGameWorld.Items = [];
MyGameWorld.CollideMapLayer = null;

MyGameWorld.create = function() {
    MyGameWorld.CollideMapLayer = MyGameMaps.loadMap();
    var item = new MyGameRock(scene, 100, 100);
    MyGameWorld.addItem(item);
};

MyGameWorld.update = function() { 
    for (var i = 0; i < MyGameWorld.Items.length; ++i)
        MyGameWorld.Items[i].update();
}

MyGameWorld.addItem = function(item) {
    scene.add.existing(item);
    scene.physics.add.existing(item);
    scene.physics.add.collider(item, MyGameWorld.CollideMapLayer);
    item.initialize();
    MyGameWorld.Items.push(item);
}