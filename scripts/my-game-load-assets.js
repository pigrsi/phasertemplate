function preload () {
    this.load.setBaseURL("assets/");
        
    // Images
    this.load.image('rock', 'images/rock.png');
        
    // Tilesets
    this.load.image('green_square', 'images/tilesets/green_square.png');
    this.load.tilemapTiledJSON('testmap_0', 'maps/testmap_0.json');
}