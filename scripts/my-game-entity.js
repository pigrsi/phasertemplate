class MyGameItem extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y, texture) {
        super(scene, x, y, texture, 0);
    }
    
    initialize() {
        this.initPhysics();
    }
    
    initPhysics() {
        this.body.setBounce(0.5, 0.4);
        
        this.body.setDrag(300, 0);
        this.body.allowDrag = false;
        
        this.body.velocity.x = 500;
    }
    
    update() {
        this.body.allowDrag = this.body.onFloor();
    }
}