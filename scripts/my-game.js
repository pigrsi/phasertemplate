var MyGame = {};
MyGame.Constants = {};
MyGame.Constants.WIDTH = 750;
MyGame.Constants.HEIGHT = 520;

var create = function() {
    MyGameWorld.create();
}

var update = function() {
    MyGameWorld.update();
};

var scene = new Phaser.Scene('Scene');
scene.preload = preload;
scene.create = create;
scene.update = update;

var MyGameSetup = {
    config: {
        type: Phaser.AUTO,
        width: MyGame.Constants.WIDTH,
        height: MyGame.Constants.HEIGHT,
        backgroundColor: '#f4ab39',
        scene: scene,
        physics: {
            default: "arcade",
            arcade: {
                debug: true,
                gravity: { y: 600 },
                fps: 60
            }
        }
    }
};
var game = new Phaser.Game(MyGameSetup.config);
